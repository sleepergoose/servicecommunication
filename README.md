# Service Communication #

Ping-Pong project based on RabbitMQ.

### Structure ###

* Pinger - console application
* Ponger - console application
* RabbitMQ.Wrapper - Class Library

### How does it work? ###

#### ������ ������ ����������: ####

�������� ����� ��������� � ping_queue
�� ������� � ������� ������� ����/����� � ��������� (� ������ ������ pong - �.�. ��������� ������ � ���������������� ����������)
����������� �������� �� 2,5 ������� � ������������ ����� ��������� � ������� pong_queue � ���������� ping
� ��� �� �����

#### ���������� ����������: ####

* Pinger (������� ������� ping_queue, ����� � pong_queue)

* Ponger (������� ������� pong_queue, ����� � ping_queue)

* RabbitMQ.Wrapper (������������� ������ � RabbitMQ � ������������� 2 ������ - ListenQueue � SendMessageToQueue)

������ ������ ���������� Ponger � ������� ���� �������, ����� �������� Pinger � ���������� ��������� + ������� ���� �������.