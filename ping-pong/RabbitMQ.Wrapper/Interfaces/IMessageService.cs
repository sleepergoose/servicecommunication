﻿using System;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageService
    {
        event MessageService.MessageDelegate MessageReceived;

        void SetMessageService(MessageScopeSettings producerSettings, MessageScopeSettings consumerSettings);

        void SendMessageToQueue(string message);

        void ListenQueue(Object model, BasicDeliverEventArgs ea);

        void Dispose();
    }
}
