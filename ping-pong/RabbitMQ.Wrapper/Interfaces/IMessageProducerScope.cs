﻿
namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducerScope
    {
        IMessageProducer MessageProducer { get; }

        void Dispose();
    }
}
