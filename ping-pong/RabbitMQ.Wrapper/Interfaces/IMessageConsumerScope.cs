﻿
namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScope
    {
        IMessageConsumer MessageConsumer { get; }

        void Dispose();
    }
}
