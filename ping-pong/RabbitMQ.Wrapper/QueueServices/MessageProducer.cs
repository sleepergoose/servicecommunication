﻿using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProducer : IMessageProducer
    {
        private readonly MessageProducerSettings _messageProducerSettings;

        private readonly IBasicProperties _basicProperties;


        public MessageProducer(MessageProducerSettings messageProducerSettings)
        {
            _messageProducerSettings = messageProducerSettings;

            _basicProperties = _messageProducerSettings.Channel.CreateBasicProperties();
            _basicProperties.Persistent = true; //  it tells RabbitMQ to save the message to disk
        }


        public void Send(string message, string type = null)
        {
            if(string.IsNullOrEmpty(type) == false)
            {
                _basicProperties.Type = type;
            }

            var body = Encoding.UTF8.GetBytes(message);
            _messageProducerSettings.Channel.BasicPublish(_messageProducerSettings.PublicationAddress,
                                                            _basicProperties, body);
        }
    }
}
