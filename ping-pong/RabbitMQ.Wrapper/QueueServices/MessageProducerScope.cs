﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProducerScope : IMessageProducerScope
    {
        // Fields
        private readonly MessageScopeSettings _messageScopeSettings;

        private readonly Lazy<IMessageQueue> _messageQueueLazy;

        private readonly Lazy<IMessageProducer> _messageProducerLazy;

        private readonly IConnectionFactory _connectionFactory;


        // Properties
        public IMessageProducer MessageProducer => _messageProducerLazy.Value;

        private IMessageQueue MessageQueue => _messageQueueLazy.Value;


        // ctor
        public MessageProducerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
            _messageProducerLazy = new Lazy<IMessageProducer>(CreateMessageProducer);
        }


        // Methods
        private IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSettings);
        }


        private IMessageProducer CreateMessageProducer()
        {
            return new MessageProducer(new MessageProducerSettings
            {
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(_messageScopeSettings.ExchangeType,
                                                            _messageScopeSettings.ExchangeName,
                                                            _messageScopeSettings.RoutingKey)
            });
        }


        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
