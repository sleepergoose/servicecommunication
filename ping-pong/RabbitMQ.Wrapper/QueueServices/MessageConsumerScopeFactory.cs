﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumerScopeFactory : IMessageConsumerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }


        public IMessageConsumerScope Open(MessageScopeSettings messageScopeSettings)
        {
            return new MessageConsumerScope(_connectionFactory, messageScopeSettings);
        }


        public IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings)
        {
            var messageConsumerScope = Open(messageScopeSettings);
            messageConsumerScope.MessageConsumer.Connect();

            return messageConsumerScope;
        }
    }
}
