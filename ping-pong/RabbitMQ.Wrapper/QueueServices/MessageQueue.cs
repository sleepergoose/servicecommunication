﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageQueue : IMessageQueue
    {
        private readonly IConnection _connection;


        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }


        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
            : this(connectionFactory)
        {
            DeclareExchange(messageScopeSettings.ExchangeName, messageScopeSettings.ExchangeType);

            if(messageScopeSettings.QueueName != null)
            {
                BindQueue(messageScopeSettings.QueueName, messageScopeSettings.ExchangeName, messageScopeSettings.RoutingKey);
            }
        }


        public IModel Channel { get; set; }


        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }


        public void BindQueue(string queueName, string exchangeName, string routingKey)
        {
            Channel.QueueDeclare(queue: queueName, durable: true, exclusive: false, autoDelete: false);
            Channel.QueueBind(queue: queueName, exchange: exchangeName, routingKey: routingKey);
        }


        public void Dispose()
        {
            Channel?.Close();
            _connection?.Close();
        }
    }
}
