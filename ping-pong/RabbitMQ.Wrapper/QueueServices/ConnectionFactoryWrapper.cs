﻿using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    // Since RabbitMQ.Client.ConnectionFactory is a sealed class
    // it is necessary  to use a wrapper class 
    public class ConnectionFactoryWrapper
    {
        public RabbitMQ.Client.ConnectionFactory ConnectionFactory { get; set; }

        public ConnectionFactoryWrapper(Uri uri)
        {
            ConnectionFactory = new RabbitMQ.Client.ConnectionFactory()
            {   
                Uri = uri,
                AutomaticRecoveryEnabled = true,
                TopologyRecoveryEnabled = true,
                RequestedConnectionTimeout = TimeSpan.FromSeconds(15),
                SocketReadTimeout = TimeSpan.FromSeconds(10),
                SocketWriteTimeout = TimeSpan.FromSeconds(10),
                NetworkRecoveryInterval = TimeSpan.FromSeconds(30),
            };
        }
    }
}
