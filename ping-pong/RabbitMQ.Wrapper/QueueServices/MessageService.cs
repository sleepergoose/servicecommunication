﻿using System;
using System.Text;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageService : IMessageService
    {
        private IMessageProducerScope _messageProducerScope;
        private IMessageConsumerScope _messageConsumerScope;

        private readonly IMessageProducerScopeFactory _messageProducerScopeFactory;
        private readonly IMessageConsumerScopeFactory _messageConsumerScopeFactory;


        public delegate void MessageDelegate(string message);

        public event MessageDelegate MessageReceived;


        public MessageService(IMessageProducerScopeFactory messageProducerScopeFactory,
            IMessageConsumerScopeFactory messageConsumerScopeFactory)
        {
            _messageProducerScopeFactory = messageProducerScopeFactory;
            _messageConsumerScopeFactory = messageConsumerScopeFactory;
        }


        public void SetMessageService(MessageScopeSettings producerSettings, MessageScopeSettings consumerSettings)
        {
            _messageProducerScope = _messageProducerScopeFactory.Open(producerSettings);
            
            _messageConsumerScope = _messageConsumerScopeFactory.Connect(consumerSettings);

            _messageConsumerScope.MessageConsumer.Received += ListenQueue;
        }


        public void SendMessageToQueue(string message)
        {
            try
            {
                _messageProducerScope.MessageProducer.Send(message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public void ListenQueue(Object model, BasicDeliverEventArgs ea)
        {
            var body = ea.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);

            MessageReceived?.Invoke(message);

            _messageConsumerScope.MessageConsumer.SetAcknowledge(deliveryTag: ea.DeliveryTag, processed: true);
        }


        public void Dispose()
        {
            _messageConsumerScope?.Dispose();
        }
    }
}
