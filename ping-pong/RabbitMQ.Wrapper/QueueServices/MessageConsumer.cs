﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Interfaces;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumer : IMessageConsumer
    {
        private readonly MessageConsumerSettings _messageConsumerSettings;

        private readonly EventingBasicConsumer _consumer;


        public MessageConsumer(MessageConsumerSettings messageConsumerSettings)
        {
            _messageConsumerSettings = messageConsumerSettings;
            _consumer = new EventingBasicConsumer(_messageConsumerSettings.Channel);
        }


        public event EventHandler<BasicDeliverEventArgs> Received
        {
            add => _consumer.Received += value;
            remove => _consumer.Received -= value;
        }


        public void Connect()
        {
            if(_messageConsumerSettings.SequentialFetch == true)
            {
                // BasicQos method with the prefetchCount = 1 setting tells RabbitMQ not to give more than one message to a worker at a time
                // in other words, don't dispatch a new message to a worker until it has processed and acknowledged the previous one.
                // Instead, it will dispatch it to the next worker that is not still busy (https://www.rabbitmq.com/tutorials/tutorial-two-dotnet.html)
                _messageConsumerSettings.Channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            }

            _messageConsumerSettings.Channel.BasicConsume(queue: _messageConsumerSettings.QueueName,
                                                          autoAck: _messageConsumerSettings.AutoAcknowledge, 
                                                          consumer: _consumer);
        }


        public void SetAcknowledge(ulong deliveryTag, bool processed)
        {
            if (processed == true )
            {
                _messageConsumerSettings.Channel.BasicAck(deliveryTag: deliveryTag, multiple: false);
            }
            else
            {
                _messageConsumerSettings.Channel.BasicNack(deliveryTag: deliveryTag, multiple: false, requeue: true);
            }
        }
    }
}
