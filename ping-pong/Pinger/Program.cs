﻿using System;
using RabbitMQ.Client;
using Pinger.Services;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.QueueServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Pinger
{
    class Program
    {
        private static IConfiguration AppConfiguration { get; set; }

        private static ServiceProvider _services;


        static void Main(string[] args)
        {
            // Dependency Injection, appsettings.json 
            Configure();


            // RabbitMQ settings
            MessageScopeSettings producerSettings = new MessageScopeSettings
            {
                ExchangeName = "PingPongExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "pong_queue",
                RoutingKey = "pong"
            };

            MessageScopeSettings consumerSettings = new MessageScopeSettings
            {
                ExchangeName = "PingPongExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "ping_queue",
                RoutingKey = "ping"
            };


            // Get the main message service with IoC
            IMessageService messageService = _services.GetService<IMessageService>();

            // Start Pinger
            PingerService pingerService = new PingerService(messageService, producerSettings, consumerSettings);


            Console.WriteLine("Press [Enter] to exit");
            Console.ReadLine();

            pingerService?.Dispose();
        }

        private static void Configure()
        {
            // appsettings
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            AppConfiguration = builder.Build();

            var uri = AppConfiguration.GetSection("RabbitUri").Value;


            // IoC
            _services = new ServiceCollection()
                .AddScoped<IConnectionFactory>(provider => 
                        new ConnectionFactoryWrapper(new Uri(uri)).ConnectionFactory)

                .AddScoped<IMessageConsumer, MessageConsumer>()
                .AddScoped<IMessageConsumerScope, MessageConsumerScope>()
                .AddSingleton<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>()

                .AddScoped<IMessageProducer, MessageProducer>()
                .AddScoped<IMessageProducerScope, MessageProducerScope>()
                .AddSingleton<IMessageProducerScopeFactory, MessageProducerScopeFactory>()

                .AddScoped<IMessageQueue, MessageQueue>()
                .AddScoped<IMessageService, MessageService>()
                
                .BuildServiceProvider();
        }
    }
}
