﻿using System;
using System.Threading;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Interfaces;

namespace Pinger.Services
{
    public class PingerService
    {
        private readonly IMessageService _messageService;

        private readonly MessageScopeSettings _producerSettings;
        private readonly MessageScopeSettings _consumerSettings;


        private int _delay = 2500;

        public int Delay
        {
            get
            {
                return _delay;
            }
            set
            {
                if (value >= 0 && value <= int.MaxValue)
                    _delay = value;
            }
        }


        public PingerService(IMessageService messageService,
                            MessageScopeSettings producerSettings,
                            MessageScopeSettings consumerSettings)
        {
            _messageService = messageService;
            _producerSettings = producerSettings;
            _consumerSettings = consumerSettings;


            // Subscribing to an event (receiving a message)
            messageService.MessageReceived += MessageReceived;

            // Set and start the message service
            messageService.SetMessageService(_producerSettings, _consumerSettings);
            
            // Send the first message
            messageService.SendMessageToQueue("ping");
        }


        private void MessageReceived(string message)
        {
            if (message == "pong")
                Console.ForegroundColor = ConsoleColor.Green;
            else
                Console.ForegroundColor = ConsoleColor.White;


            Console.WriteLine($"{DateTime.Now.ToShortDateString()} | {DateTime.Now.ToLongTimeString()} - {message}");
            Console.ForegroundColor = ConsoleColor.White;

            Thread.Sleep(_delay);

            _messageService.SendMessageToQueue("ping");
        }


        public void Dispose()
        {
            _messageService?.Dispose();
        }
    }
}
