﻿using System;
using Ponger.Services;
using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.QueueServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Ponger
{
    class Program
    {
        private static IConfiguration AppConfiguration { get; set; }

        private static ServiceProvider _services;


        static void Main(string[] args)
        {
            // Dependency Injection and appsettings.json file
            Configure();


            // RabbitMQ settings
            MessageScopeSettings producerSettings = new MessageScopeSettings
            {
                ExchangeName = "PingPongExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "ping_queue",
                RoutingKey = "ping"
            };

            MessageScopeSettings consumerSettings = new MessageScopeSettings
            {
                ExchangeName = "PingPongExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "pong_queue",
                RoutingKey = "pong"
            };


            // Get the main message service with IoC
            IMessageService messageService = _services.GetService<IMessageService>();

            // Start Pinger
            PongerService pongerService = new PongerService(messageService, producerSettings, consumerSettings);


            Console.WriteLine("Press [Enter] to exit");
            Console.ReadLine();

            pongerService?.Dispose();
        }



        private static void Configure()
        {
            // appsettings
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            AppConfiguration = builder.Build();

            var uri = AppConfiguration.GetSection("RabbitUri").Value;


            // IoC
            _services = new ServiceCollection()
                .AddScoped<IConnectionFactory>(provider =>
                        new ConnectionFactoryWrapper(new Uri(uri)).ConnectionFactory)

                .AddScoped<IMessageConsumer, MessageConsumer>()
                .AddScoped<IMessageConsumerScope, MessageConsumerScope>()
                .AddSingleton<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>()

                .AddScoped<IMessageProducer, MessageProducer>()
                .AddScoped<IMessageProducerScope, MessageProducerScope>()
                .AddSingleton<IMessageProducerScopeFactory, MessageProducerScopeFactory>()

                .AddScoped<IMessageQueue, MessageQueue>()
                .AddScoped<IMessageService, MessageService>()

                .BuildServiceProvider();
        }
    }
}
